#ifndef LOCKLESS_QUEUE_H
#define LOCKLESS_QUEUE_H

#include <atomic>
#include <ostream>
#include <thread>

#define QUEUE_SIZE 4

template <typename T>
class LocklessQueue {
  private:
    T data[QUEUE_SIZE];
    std::atomic_size_t head;
    std::atomic_size_t tail;

  public:
    LocklessQueue() {
      head = tail = 0;
    }

    LocklessQueue(const LocklessQueue&) = delete;
    LocklessQueue& operator=(const LocklessQueue&) = delete;

    void push(T target) {
      size_t headLocal = head.load(std::memory_order_relaxed);
      while ((headLocal + 1) % QUEUE_SIZE == tail.load(std::memory_order_relaxed)) {
        std::this_thread::yield();
      }

      data[headLocal] = target;
      std::atomic_thread_fence(std::memory_order_release);
      head.store((headLocal+1) % QUEUE_SIZE, std::memory_order_relaxed);
    }

    bool ready() {
      return head.load(std::memory_order_relaxed) != tail.load(std::memory_order_relaxed);
    }

    T pop() {
      size_t tailLocal = tail.load(std::memory_order_relaxed);
      std::atomic_thread_fence(std::memory_order_acquire);
      T result = data[tailLocal];
      tail.store((tailLocal + 1) % QUEUE_SIZE, std::memory_order_relaxed);
      return result;
    }

    friend std::ostream& operator<<(std::ostream& os, const LocklessQueue& q) {
      return os << "Head = " << q.head.load() << " Tail = " << q.tail.load();
    }
};
#endif
