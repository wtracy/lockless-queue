#include <chrono>
#include <thread>

#include "gtest/gtest.h"

#include "../lockless-queue.hpp"

void doWriteSeveral(LocklessQueue<int>* lq, std::vector<int> targets) {
  for (const auto i : targets)
    lq->push(i);
}

void doWrite(LocklessQueue<int>* lq, int target) {
  lq->push(target);
}

void doRead(LocklessQueue<int>* lq, int expected) {
  while (!lq->ready())
    std::this_thread::yield();

  EXPECT_EQ(lq->pop(), expected);
}

void doReadSeveral(LocklessQueue<int>* lq, std::vector<int> expected) {
  for (const auto i : expected) {
    while (!lq->ready())
      std::this_thread::yield();
    EXPECT_EQ(lq->pop(), i);
  }
}

TEST(MultiThreadedTest, WriteFirst) {
  LocklessQueue<int> lq;

  std::thread write(doWrite, &lq, 42);
  write.join();

  std::thread read(doRead, &lq, 42);
  read.join();
}

TEST(MultiThreadedTest, ReadFirst) {
  LocklessQueue<int> lq;

  std::thread read(doRead, &lq, 42);
  std::this_thread::sleep_for(std::chrono::seconds(1));

  std::thread write(doWrite, &lq, 42);

  read.join();
  write.join();
}

TEST(MultiThreadedTest, Overfill) {
  LocklessQueue<int> lq;

  std::thread write(doWriteSeveral, &lq, std::vector<int> {5, 7, 8, 3, 1, 5, 9});
  std::this_thread::sleep_for(std::chrono::seconds(1));
  std::thread read(doReadSeveral, &lq, std::vector<int> {5, 7, 8, 3, 1, 5, 9});

  write.join();
  read.join();
}
