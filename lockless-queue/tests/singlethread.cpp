#include "../lockless-queue.hpp"

#include "gtest/gtest.h"

TEST(SingleThreadedTest, SimpleReadWrite) {
  LocklessQueue<int> lq;

  EXPECT_FALSE(lq.ready());

  lq.push(42);
  EXPECT_TRUE(lq.ready());
  EXPECT_EQ(lq.pop(), 42);
  EXPECT_FALSE(lq.ready());
}

TEST(SingleThreadedTest, FillDrain) {
  LocklessQueue<int> lq;

  EXPECT_FALSE(lq.ready());
  for (int i = 0; i < 3; ++i)
    lq.push(i);

  for (int i = 0; i < 3; ++i) {
    EXPECT_TRUE(lq.ready());
    EXPECT_EQ(lq.pop(), i);
  }

  EXPECT_FALSE(lq.ready());
  for (int i = 0; i < 3; ++i) {
    lq.push(i+42);
  }

  for (int i = 0; i < 3; ++i) {
    EXPECT_TRUE(lq.ready());
    EXPECT_EQ(lq.pop(), i+42);
  }
  EXPECT_FALSE(lq.ready());
}
